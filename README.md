# Sachakshu- Having eyes

## Problems faced by blinds 
1. They are dependent on others to get the information about the public transport, or to get the right directions. 

2. Since the GPS applications or modules that we use or are available are dependent on the Google Maps, which is a test version i.e. the Beta version of maps is available
Secondly, due to the limitations enforced by the Government of India some of the features of the google maps are not available hence causing a problem when it comes to getting the right directions. 

3. The state public transport routes are often changing so when it comes to searching for a particular bus route we generally get routes based on the ‘static info’ as these small-small changes are not sent to the application so that the application update its data and provide the right route 

4. Many times the blinds face a very common problem that using there stick they are many a time not able to figure out an obstacle, and they may get hurt or many a time may forget their way. Since we are living in a country like India where people have made their shops on the footpaths also which makes it very difficult for anyone to walk, Google Map or GPS would be telling that there is a way as they don’t know about that construction. 
 
## Our Idea:
Our objective is to design a Blind System compatible with the public transport.
a. Our idea is design a Blind System to help the blind using a device which will be in form of a wrist watch, and will be having a mechanism in which if he/she gives a voice command, the bus module of the our system will get actuated and will start the read/write module of the system which will make changes accordingly in the RFID receiver present in the wristwatch module of our system and can be feed to the ears of the user either by the use of the Bluetooth or Earphones.

b. We are making an android application with the following features:
	1. Android application to control all the hardware through voice commands
	2. Android application to convert the received information into voice signals so that all the information can be made audible to the visually challenged persons.

c. The problem of the obstacle can be resolved as we can add an Ultrasonic Transceiver (HC SR-04).

## Currently What We Are Doing
1. We are using passive RFID that would store the information of the bus since we are using a passive RFID so we can’t program it (in future it can be replaced by a read/write Active RFID).

2. So currently at the receiver end, we have attached an RFID reader which will read the contents of the RFID and would take the last 3 bit of the received data to be the bus number and which would then be send to the android text-speech application and hence all the data will be made audible to the person.

3. Since we are designing it for the blinds so we have given a voice controlled UI that will respond the voice commands given by the user.

## Use Cases
1. When a visually disabled person wants to use a bus as mode of transport, he/she may just give the voice command, this may actuate the trigger mechanism of the bus which in turn will lead to the bus module system transmitting all the information to the receiver, hence feeding all info to the ears of the visually disabled person.

2. Now for the obstacle detection, the person moving would get an idea that whether there is any obstacle or not. Suppose he/she has to find where the bus is standing or where he/she has to keep his foot so as to get on to the bus.

3. Now talking about the “illegal” construction, we can’t stop it, neither we can keep a track of it or that the way that was there until last week is that way still open or is ‘blocked’?, so by the use of the obstacle detection we can inform the ‘blind’ person about the inability of that route.

## Shop Stoppers
1. Since we are connecting our system with the bus internal circuit (circuit designed for handling the dashboard), hence any change in the bus route will get reflected in our system.

2. The obstacle detection can lead to a decrease in a number of injuries that results due to these obstacles.

3. The wrist module can be extended to the other modes of transport such as Indian Railways, metros, etc.

4. Now since this system needs to be future compatible, the future is of the Cloud Computing or the IoT (Internet of Things). So we will be incorporating that to monitor our system and also to interconnect all the SYST­­­­­­EM nodes together.

## Future Plans
1. As the user gives a voice command to the android,, the system will start to transmit an actuating system which can be received by any bus module within the vicinity, and hence all the information will be stored in the  read/write active RFID by the bus module which can then be received and decoded by the RFID reader and fed to the ears of the user. The wrist module will have a clock that will wait for 60 seconds for the bus module signal. After 60 seconds, a message will be aired informing the user that there are no buses around

2. Since the use of Ultrasonic Transceiver has a lot of limitations, so instead we will be using a Camera and OpenCV to enhance and implement the concept of Obstacle Detection.

3. In our system, the bus module of our system will constantly be looking for an actuating signal, and as soon as it receives the actuating signal it will actuate the read/write module (connected to the internal system responsible for managing the Dashboard of the bus) which will be storing all the required information in the read/write active RFID.
Suppose the user due to any reasons loses/misplaces his wrist module, a separate system can be used to track the wrist module, the system would be like that on giving a voice command on his mobile or on an external hardware will activate a “constant audio sound producing” module, hence making it easy for the person to follow the sound and locate.

4. Since for the bus system, we will not be requiring any external power source, as the buses already contain one, but the external source will be required at the wrist module end. In order to solve the problem of power, we will implement a 3 stage process, at the first stage we will be providing a 2000-3000mAh battery, which will last up to 10 hours for a circuit that will be drawing a 500mA current when the data is being received and decoded, and 10uA when it is not receiving any information. In the second stage we will provide a USB charger point, so to charge the receiver system at regular interval. The person can also charge the system by connecting a power bank with it through the USB port. In the third stage, we will provide a solar panel system to facilitate charging on the go through Solar Energy.

5. We are aiming to develop an application that will be interacting with the State Public Transport server and will be working as a messaging website which will be sending information as messages (this feature is currently used  by many of the Airlines)

6. We are planning to replace our Android Voiced controlled system with Amazon Alexa.

7. We are proposing an application that would have a direct link with the Indian Regional Navigation Satellite System (IRNSS), so as to get a more accurate tracking of the buses, or any other kind of public transport.
![image](Percentage_chart.png)

### Schematic of our initial proof of concept
<<<<<<< HEAD

=======
>>>>>>> aff1eed9f62dc8541b4f67204a0c2abaebc66ce5
![image](Sachkashu.png)
