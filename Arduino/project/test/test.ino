#include <dht.h>              //DHT11 Library
#include <SoftwareSerial.h>   //Library for connecting A6 GSM Module

char input='\0';             
String mesage,str,Str,Str1,bus_no="\0";
int count=0,gate=0,A=0,B=0;

#define echoPin1 5
#define echoPin 6
#define DHT11_PIN 7
#define IR 8
SoftwareSerial A6Module(10, 11); //RX,TX
#define trigPin1 12
#define trigPin 13

dht DHT;

//Setup function to set the basic requirnmnet
void setup() 
{
  Serial.begin(9600);
  A6Module.begin(9600);
  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);
  pinMode(trigPin1, OUTPUT);
  pinMode(echoPin1, INPUT);
  pinMode(IR,INPUT);
}

//loop function to that runs continuesly
void loop() 
{
  float d=ultrasonic();
  delay(1000);
  float d1=Ultrasonic();
  delay(1000);
  if(gate==0)
  { 
    gate=digitalRead(IR);
    if(gate==1)
    {
      sendSMS("Gate was opened");
      if(d>2 && d<400)
      {
        A=1;
        if(d1>2 && d1<400)
        {
          B=1;
          
          if(A==1 && B==1)
          {
            count++;
          }
          else if(B==1 && A==1)
          {
            count--;
          }
        }
      }
    }
  }
  else if(gate==1)
  {
    gate=digitalRead(IR);
    if(gate==0)
    {
      str="Bus No: ";
      Str=str+bus_no;
      str="\nDoor Status: Closed";
      Str1=Str+str;
      str="\nPassenger Count: ";
      mesage=Str1+str+count;
      sendSMS(mesage);
    }
  }
  delay(1000);
}

//function to send SMS
void sendSMS(String a) 
{
  A6Module.println("AT+CMGF=1");
  delay(2000);
  A6Module.print("AT+CMGS=\"+919910701948");
  A6Module.print(char(34));  // "
  A6Module.print(char(13));  // CR
  A6Module.print('\r');  // hex equivalent of newline
  delay(2000);
  A6Module.print(a);
  delay(500);
  A6Module.println (char(26));  //ctrl_z
  feedback();
}

//function to return the distance from the first HC SR-04
float ultrasonic()
{
  float duration, distance;
  float speed;
  
  digitalWrite(trigPin, LOW); 
  delayMicroseconds(2);
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);

  duration = pulseIn(echoPin, HIGH);
  speed = 331.4 + (0.606 * DHT.temperature) + (0.0124 * DHT.humidity);
  distance = (duration / 2) * (speed / 10000);
  
  return distance;
}

//function to return the distance from the second HC SR-04
float Ultrasonic()
{
  float duration, distance;
  float speed;
  
  digitalWrite(trigPin1, LOW); 
  delayMicroseconds(2);
  digitalWrite(trigPin1, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin1, LOW);

  duration = pulseIn(echoPin1, HIGH);
  speed = 331.4 + (0.606 * DHT.temperature) + (0.0124 * DHT.humidity);
  distance = (duration / 2) * (speed / 10000);
  
  return distance;
}

//function to return the print the error from the GSM module
void feedback()
{
  while(Serial.available())
  {
    Serial.print(A6Module.read());
  }
}

//function to make call using the GSM module
void makeCall() 
{
  A6Module.println("ATD+91910701948");
  delay(20000);
  A6Module.println("ATH");  //end call
}

//read the mobile  number from the Serial monitor
int readSerial(char result[]) 
{
    int i = 0;
    while (1) 
    {
      while (Serial.available() > 0) 
      {
        char inChar = Serial.read();
        if (inChar == '\n')
        {
          result[i] = '\0';
          Serial.flush();
          return 0;
        }
        if (inChar != '\r') 
        {
          result[i] = inChar;
          i++;
        }
      }
    }
}

//function to print the contents of the received message
void RecieveMessage()
{
  A6Module.println("AT+CNMI=2,2,0,0,0"); // AT Command to receive a live SMS
  delay(1000);
}
