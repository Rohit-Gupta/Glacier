#include <SoftwareSerial.h>

SoftwareSerial A6Module(10, 11); //RX,TX
char input='\0';

void setup() 
{
  A6Module.begin(9600);
  Serial.begin(9600);
  delay(500);
  Serial.print("SMS Sender");
}

void loop()
{
  if (Serial.available() > 0) 
  {
    input = Serial.read();
  }
  if (input == 't') 
  {
    sendMSM("A6 SMS test");
    input = '\0';
  } 
  else if (input == 'c') 
  {
    makeCall();
    input = '\0';
  }
}

void sendMSM(String a) 
{
  A6Module.println("AT+CMGF=1");
  delay(2000);
  A6Module.print("AT+CMGS=\"+919910701948");
  A6Module.print(char(34));  // "
  A6Module.print(char(13));  // CR
  A6Module.print('\r');  // hex equivalent of newline
  delay(2000);
  A6Module.print(a);
  delay(500);
  A6Module.println (char(26));  //ctrl_z
  feedback();
}

void makeCall() 
{
  A6Module.println("ATD+91910701948");
  delay(20000);
  feedback();
  A6Module.println("ATH");  //end call
}

void feedback()
{
  while(Serial.available())
  {
    Serial.print(A6Module.read());
  }
}

void RecieveMessage()
{
  A6Module.println("AT+CNMI=2,2,0,0,0"); // AT Command to receive a live SMS
  delay(1000);
}
