#include<SoftwareSerial.h>    /*library to connect bluetooth*/
#include<string.h>            /*for strcpy()*/
#include<dht.h>               /*for using DHT11*/
 
#define trigPin 3     /*trigPin is connected to pin 3 of arduino*/
#define echoPin 4     /*echoPin is connected to pin 4 of arduino*/
#define DHT11_PIN 7   /*DHT11 is connected to pin 7 of arduino*/

SoftwareSerial bluetooth(12, 13);
dht DHT;

char b[12];
String voice;
char a[3][12] = {
  {'3', '4', '0', '0', '6', 'D', 'D', '0', 'E', '3', '6', 'A'},
  {'4', '0', '0', '0', '2', 'D', '9', 'D', '2', 'F', 'D', 'F'},
  {'3', '6', '0', '0', '3', '2', 'F', 'I', '1', 'D', 'E', 'C'}    /*Value of 3 different RFID tags*/
};
int flag = 0, i=0, j=0, count,distance=0;

void setup()
{
  bluetooth.begin(9600);
  Serial.begin(9600);;
  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);
  pinMode(DHT11_PIN, OUTPUT);
  pinMode(12,INPUT);
  pinMode(13,OUTPUT);
  bluetooth.flush();
  Serial.flush();
}

void loop()
{
  bluetooth.listen();
  while (bluetooth.available() > 0)
  { 
    //Check if there is an available byte to read
    delay(10); //Delay added to make thing stable 
    char c = bluetooth.read(); //Conduct a serial read
    if (c == '#') 
    {
      break;
    } 
    //Exit the loop when the # is detected after the word
    voice += c; 
  }  
  Serial.println(voice);
  if (voice.length() > 0) 
  {
    if(voice=="*ultrasonic")
    {
      for(i=0;i<4;i++)       /*Utrasonic transceiever active for 60 seconds*/
      {
        distance=Ultrasonic();    
        Serial.println(distance); 
        delay(5000);  
      }
    }
    else if(voice=="*bus")
    {
      bluetooth.print("*");
      bluetooth.print("The comming bus number is ");
      Bus();
      delay(1000);  
    }
    else
    {
      Serial.println("Not Able to get your command");
      bluetooth.print("*Not Able to get your command#");
    }
    voice="";
  }
  bluetooth.flush();
  Serial.flush();
}

int Ultrasonic()    /*Function calculating the Distance using HC SR-04 and DHT11 */
{
    int distance;
    float duration,speed;

    digitalWrite(trigPin, LOW);
    delayMicroseconds(2);
    digitalWrite(trigPin, HIGH);
    delayMicroseconds(10);
    digitalWrite(trigPin, LOW);

    duration = pulseIn(echoPin, HIGH);
    speed = 331.4 + (0.606 * DHT.temperature) + (0.0124 * DHT.humidity);
    distance = duration / 29 / 2;

    if (distance < 401 && distance > 2)
    {
      bluetooth.print("*There is an obstacle in front of you ");
      bluetooth.print(distance);
      bluetooth.print(" cm away#");
      return distance;
    }
}

int Bus()     /*/Function for reading the RFID tag value*/
{
  if(Serial.available())  /* Check if there is incoming data in the RFID Reader Serial Buffer.*/
  {
    count = 0; 
    /* Reset the counter to zero keep reading Byte by Byte from the Buffer till the RFID Reader Buffer is empty or till 12 Bytes (the ID size of our Tag) is read */
    while(Serial.available() && count < 12) 
    {
      b[count] = Serial.read(); /* Read 1 Byte of data and store it in the input[] variable */
      count++;                  /* increment counter*/
      delay(5);
    }
    /* When the counter reaches 12 (the size of the ID) we stop and compare each value of the input[] to the corresponding stored value */
    if(count == 12)
    {
      count =0; // reset counter varibale to 0
      flag = 1;
      /* Iterate through each value and compare till either the 12 values are all matching or till the first mistmatch occurs */
      while(j<3)
      {
        while(count<12 && flag !=0 )  
        {
          if(b[count]==a[j][count])
            flag = 1; // everytime the values match, we set the flag variable to 1
          else
            flag= 0; 
          /* if the ID values don't match, set flag variable to 0 and stop comparing by exiting the while loop */
          count++; // increment i
        }
        j++;
      }
    }
    if(flag == 1) // If flag variable is 1, then it means the tags match
    {
      for(int k=1;k<12;k++)
      {
       /*To print the last 3 digits of the retreived value of the RFID tag */
       if(k<9)
       {
          continue;
       }
       else
       {
          bluetooth.print(b[k]);
       }
      }
      bluetooth.print("#");
    }
    for(count=0; count<12; count++) 
    {
      b[count]= 'F'; /*to fill the array with default values*/
    }
    count = 0; /* Reset counter variable*/
  }
}
