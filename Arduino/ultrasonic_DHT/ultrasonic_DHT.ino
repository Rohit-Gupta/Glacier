#include <dht.h>
#include <SoftwareSerial.h>

SoftwareSerial mySerial(9, 10);

#define trigPin 13
#define echoPin 6
#define DHT11_PIN 7

dht DHT;

void setup() {
  Serial.begin (9600);
  mySerial.begin(9600);
  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);
}

void loop() {
  
  float duration, distance;
  float speed;
//  float speed;
  
  digitalWrite(trigPin, LOW); 
  delayMicroseconds(2);
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);

  duration = pulseIn(echoPin, HIGH);
  speed = 331.4 + (0.606 * DHT.temperature) + (0.0124 * DHT.humidity);
  distance = (duration / 2) * (speed / 10000);
  
  if (distance >= 400 || distance <= 2){
   Serial.write("Distance = ");
    Serial.print(duration);
    Serial.write("Out of range");
  }
  else 
  if(distance>2 && distance<400)
  {
    Serial.write("Distance = ");
    Serial.print(distance);
    //Serial.print(distance);
    Serial.write(" cm");
    Serial.println();
    delay(1000);
  }
  delay(1000);
}
